Partie 1 : Gestion des utilisateurs, groupes et droits

1.1 Manipulation : créer un compte utilisateur

- Créer un compte utilisateur portant le nom "wilder", pour lequel le répertoire de travail est /home/wilder/, faisant partie du groupe "wilder" et n'ayant pas de super privilèges :
-> groupadd wilder
-> useradd -m -d /home/wilder -g wilder wilder
 - Créer un répertoire "/home/share" et donner les droits à l'utilisateur "wilder" en lecture, écriture et exécution :
-> mkdir /home/share
-> chmod 770 /home/share
-> chown wilder:wilder /home/share

- Créer un fichier "passwords.txt" dans /home/share" en passant par le biais du compte "wilder". Sans faire de chown, l'utilisateur "wilder" doit pouvoir avoir accès au fichier en lecture et écriture :
-> touch /home/share/passwords.txt
-> chmod u+rw /home/share/passwords.txt

- Créer un groupe "share" : 
-> groupadd share

- Ajouter l'utilisateur "wilder" au groupe "share" :
-> usermod -aG share wilder

- Ajouter le groupe "share" à ton propre compte utilisateur (celui sur lequel tu es connecté) :
-> usermod -aG share nassima

- Mettre les droits du groupe "share" en écriture et lecture sur le répertoire "/home/share" ainsi que tous les fichiers et sous répertoires qui se trouvent à l'intérieur :
-> chmod -R g+rw /home/share

Chiffrer le répertoire "/home/share" pour qu'il ne soit pas lisible autrement qu'en le déchiffrant par un mot de passe ;

Customer la session de l'utilisateur "wilder" en lui ajoutant un message qui s'affiche une fois connecté sous la forme suivante


1.2 Question : quelles sont les adresses ip propre au conteneur ?

En général, les conteneurs et les réseaux de conteneurs utilisent souvent des plages d'adresses IP privées telle que 10.0.0.0/8 pour leur configuration réseau. La plage 10.0.0.0/8 est souvent préférée en raison de sa grande taille. J'ai souvens vu des adresse ip commençant par 172 ou 192 pour la virtualisation et en 10. pour les conteneurs.

