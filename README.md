# Checkpoint 1









# Introduction : 

Le premier checkpoint a pour but de permettre à chacun de vérifier s'il a bien assimilé les compétences enseignées jusqu'à présent. On peut se référer aux ressources disponibles, mais l'exercice se fait individuellement. La rapidité d'exécution n'est pas cruciale, l'important est de comprendre les concepts. Le formateur évalue le travail de chaque participant pour les aider à progresser.









![Alt text](image.png)











📝 Objectifs
Répondre à un maximum de question du challenge
Valider les compétences acquises durant ces dernières semaines de formations










🤓 Contexte de ce checkpoint
Dans ce checkpoint, il faut travailler sur un conteneur (LXC) sous Debian 12, qui est vierge de tout paquet logiciel. 
Normalement, un "snapshot" nommé "default" est mis à disposition pour restaurer l'environnement de travail à cet état.







                                                    🚀🚀🚀🚀🚀🚀🚀 LET'S GO 🚀🚀🚀🚀🚀🚀🚀