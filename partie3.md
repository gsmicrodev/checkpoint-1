#!/bin/bash

# Variables d'identification
conteneur_ip="2a01:4f8:141:53ea::153"  # Adresse IP du conteneur
utilisateur="wilder"                   # Nom d'utilisateur sur le conteneur

# Chemin complet de la clé privée SSH de wilder
cle_privee="/home/nassima/.ssh/id_rsa"

# Vérification si la clé privée existe, sinon en générer une
if [ ! -f "$cle_privee" ]; then
    echo "Clé privée inexistante. Génération d'une nouvelle clé..."
    ssh-keygen -t rsa -b 2048 -f "$cle_privee" -N ""
fi

# La clé privée SSH est ajoutée à l'agent SSH
ssh-add "$cle_privee"

# Utilisation de ssh pour se connecter au conteneur
ssh "$utilisateur@$conteneur_ip"

3.2 Question : Qu'est censé faire le script Bash ci-dessous ?

MAX=95
EMAIL=wilder@email.sh

USE=$(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage ""}')
if [ $USE -gt $MAX ]; then
	echo "Percent used: $USE" | mail -s "Running out of CPU power" $EMAIL
fi

Ce script Bash a pour objectif de monitorer l'usage du processeur (CPU) sur un système et d'envoyer une notification par e-mail si l'utilisation du CPU excède un seuil prédéfini. Cela permet de surveiller et d'être alerté en cas de charge CPU excessive.

3.2 Question : Est-ce que l'utilisateur "wilder" va pouvoir installer des paquets logiciels tels que apache ou nginx ? Que la réponse soit oui ou non, expliquer pourquoi en quelques mots

L'utilisateur "wilder" ne peut pas installer des paquets logiciels tels que apache ou nginx car il n'a pas les droits root (super utilisateur).
Il pourra cependant utiliser la commande sudo pour s'administrer ces droits ce qui implique de taper la commande sudo à chaques manipulations.
Mais ne pas oublier qu'un grand pouvoir implique de grandes responsabilités ⚡