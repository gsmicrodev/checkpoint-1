Partie 5 : Administration système et réseau
5.1 Manipulation : installer un outil de surveillance de l'activité réseau d'un conteneur
Se connecter au conteneur distant avec un compte ayant les droits super utilisateur (sudo)
Chercher et installer un outil, accessible uniquement en ligne de commande, permettant de monitorer le trafic sur une interface réseau spécifique du conteneur
Monitorer l'interface "eth0"
Faire un rapport d'analyse (décrire ce que tu vois et quel outil tu as utilisé)