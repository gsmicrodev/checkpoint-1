4.1 Qu'est-ce que l'infrastructure as code (IaC) ?
L'infrastructure as code (IaC) est une approche pour gérer l'infrastructure informatique en utilisant des fichiers de code au lieu de configurations manuelles. 💻 

4.2 Est-ce que Docker est une nécessité dans le milieu DevOps ? (Expliquer la réponse)
Docker n'est pas une nécessité absolue, mais il est largement utilisé en DevOps pour la gestion des conteneurs, ce qui simplifie le déploiement et la gestion d'applications. 🐳

4.3 Qu'est-ce qu'une pipeline CI/CD ?
Une pipeline CI/CD est un processus automatisé qui intègre, teste et déploie du code de manière continue, de l'intégration à la livraison continue.🏭

4.4 Quel outil (logiciel) utiliserais tu pour gérer des configurations serveurs à distance ?
Ansible est un outil couramment utilisé pour gérer des configurations serveurs à distance.🤖

4.5 Que signifie le terme "scalabilité" pour le milieu DevOps ?
La scalabilité en DevOps signifie que l'infrastructure et les applications sont capables de s'adapter à la demande, en augmentant ou en réduisant les ressources en fonction des besoins.📈

4.6 Quel est le principal rôle d'un administrateur système DevOps en entreprise ?
Le principal rôle d'un administrateur système DevOps en entreprise est de garantir la stabilité, la sécurité et l'efficacité de l'infrastructure informatique, tout en automatisant les processus.👨‍💻

4.7 Quel outil (plateforme) utiliserais tu pour créer une pipeline de déploiement logiciel ?
Des outils tels que Jenkins, GitLab CI/CD, sont utilisés pour créer des pipelines de déploiement logiciel.🐸🚀

4.8 Quels types d'environnements mettrais tu en place avant une mise en production de logiciel ?
Avant une mise en production de logiciel, on mettra en place des environnements de développement, de test, de staging et de production.🔨🚧

4.9 Qu'est-ce que signifie exactement la notion d'intégration continue (CI) ?
L'intégration continue (CI) implique de fusionner fréquemment le code des développeurs dans un référentiel commun, suivi d'une automatisation des tests pour détecter les problèmes rapidement.🔄 

4.10 Que signifie la notion de "provisionning" pour un administrateur système DevOps ?
La notion de "provisionning" pour un administrateur système DevOps consiste à créer et configurer des serveurs et des ressources de manière automatisée en fonction des besoins.🚚🧰 
