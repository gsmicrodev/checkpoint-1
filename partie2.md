2.1 Manipulation : sécuriser l'environnement distant

- Modifier le port d'accès SSH du conteneur et faire en sorte que ça ne soit plus le port 22 :
-> sudo nano /etc/ssh/sshd_config
Rechercher la ligne qui commence par "Port" (elle est réglée sur 22 par défaut) et modifier le numéro de port : -> Port 2222
Enregistrer et fermer le fichier de configuration
Redémarrer le service SSH pour appliquer les modifications : 
-> sudo service ssh restart


Bloquer tous les ports du conteneur à l'aide d'un pare-feu logiciel :
-> sudo apt-get install ufw
-> sudo ufw enable
-> sudo ufw default deny incoming

Autoriser l'accès depuis le port SSH que tu viens de définir précédemment à l'aide du pare-feu logiciel :
-> sudo ufw allow 2222/tcp
-> sudo ufw enable

Vérification des règles du pare-feu pour s'assurer que le port SSH (2222) est correctement autorisé : 
-> sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
2222/tcp                   ALLOW       Anywhere                  
2222/tcp (v6)              ALLOW       Anywhere (v6)  

2.2 Question : quel autre moyen simple peux-tu mettre en œuvre pour augmenter la sécurité du conteneur ?

On peut utiliser la 2FA pour SSH, c'est comme un double verrouillage de porte. En gros, vous avez besoin de deux clés différentes pour entrer, ce qui rend l'accès beaucoup plus sécurisé. Cela peut être fait avec un mot de passe et un code temporaire de votre téléphone. C'est comme avoir une double protection pour votre compte.
"Double la sécurité, double la tranquillité d'esprit !" 😄